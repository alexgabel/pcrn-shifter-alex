- **Notebook: *shifter_test.ipynb***

Used for testing the combined network.

- **Report 1: VLN / Notebook: *shifter_VLN.ipynb***

An analysis of the effect of the Very Large Number (VLN) on the output of the combined shifter and PCRN was performed. 

- **Report 2: Varying the Base Stimulation**

For various values of the base stimulation, the mean activity of the winning populations was recorded. 

- **Report 3: Deducing External Input from Subpopulation’s Mean Activities / Notebook: *shifter_ambig.ipynb***

- **Report 4: Learning of the Shifter Network / Notebook: *shifter_analysis.ipynb***

In order to understand how Boltzmann machines acquire their final weights - and to determine whether the total number of sweeps is in fact ideal - an investigation was performed regarding the evolution of the weights during training. The usual algorithm proposed by Hinton (1986) was used for 20,000 sweeps on sequences of 10 bits.

- **Report 5A: Hidden Unit Inputs for given Samples / Notebook: *shifter_patterns.ipynb***

- **Report 5B: Threshold and Deviations from Idealized Feature Detectors / Notebook: *shifter_patterns.ipynb***

- **Report 5C: Comparing Different Hidden Units / Notebook: *shifter_patterns2.ipynb***

- **Report 6: Multi-bit Samples / Notebook: *shifter_patterns2.ipynb***

- **Report 7: Shortest Blocks / Notebook: *pcrn_blocks.ipynb* and *shortest_blocks_test.ipynb***

- **Report 8: Learning with PCRN / Notebook: *learn_weights_test.ipynb***