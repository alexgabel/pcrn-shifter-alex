"""
Author: Sergi Mas-Pujol
Last update: 06/06/2019

The code requieres Python3.6

"""

import numpy as np
from scipy import sparse
import matplotlib.pyplot as plt

plt.style.use('ggplot')

import time as time

import math
from multiprocessing import Pool

# Created files in order to re-use the code
import samplesGenerator as sg
import runNet as runNet

import sys


def meanActivity_winning(tSteps, Vt, bitsPerPatt):
    """
    Counting the mean number of active neurons in each population when it is winning

    Input:
        * tSteps
        * V_temporal
        * bitsPerPatt

    Output:
        * activeNeurons -> 3xtSteps -> number of active nuerons at each tStep
        * populationWinning -> 3xtSteps -> where each position = 1 if this population was winning
        * num_tSteps_populationWinning -> 1x3 -> sum of tSteps of each population winning
        * proportionActive -> 1x3 -> mean number of active neurons when each population was winning
    """

    activeNeurons = np.zeros([3, tSteps])
    populationWinning = np.zeros([3, tSteps])

    for i in range(tSteps):

        activeNeurons[0,i] = np.sum(Vt[0:int(bitsPerPatt), i])
        activeNeurons[1,i] = np.sum(Vt[int(bitsPerPatt):2*int(bitsPerPatt), i])
        activeNeurons[2,i] = np.sum(Vt[2*int(bitsPerPatt):3*int(bitsPerPatt), i])

        currentPopulationWinning = np.argmax(activeNeurons[:, i])

        populationWinning[0,i] = currentPopulationWinning == 0
        populationWinning[1,i] = currentPopulationWinning == 1
        populationWinning[2,i] = currentPopulationWinning == 2


    activeNeurons_populationWinning_total = np.sum(activeNeurons * populationWinning, 1)
    num_tSteps_populationWinning = np.sum(populationWinning, 1)

    proportionActive = activeNeurons_populationWinning_total / num_tSteps_populationWinning
    #proportionActive = np.divide(activeNeurons_populationWinning_total,
                                # num_tSteps_populationWinning,
                                # out=np.zeros_like(activeNeurons_populationWinning_total),
                                # where=num_tSteps_populationWinning!=0)

    return activeNeurons, populationWinning, num_tSteps_populationWinning, proportionActive



# Compute the sigmoid function given a sigle value with betha=1
def sigmoid(value):
    return 1/(1+np.exp(-value))


# Function that convert the result from the shifter into a valid values for the PCRN
def hExt_pcrn(h_shifter, h0):

#     h_base = max(h_shifter)
    h_base = h0 # Comming from the initial PCRN implementation
    num_population = 3 # Extract it from the available data in the code
    hExtDeltaPerPatt = np.zeros((3,))

    for i in range(num_population):
        if (h_shifter[i] != max(h_shifter)):
            hExtDeltaPerPatt[i] = (h_base-h_shifter[i])*0.01
        else:
            hExtDeltaPerPatt[i] = 0


    # Sanity check: h_pcrn and hExtDeltaPerPatt > 0
    for i in range(num_population):
        if (hExtDeltaPerPatt[i]<0):
            print('ERROR: Required positive external stimulaton')
            sys.exit()

    return hExtDeltaPerPatt



def smooth(x, window_len ,window):
    """smooth the data using a window with requested size.

    This method is based on the convolution of a scaled window with the signal.
    The signal is prepared by introducing reflected copies of the signal
    (with the window size) in both ends so that transient parts are minimized
    in the begining and end part of the output signal.

    input:
        x: the input signal
        window_len: the dimension of the smoothing window; should be an odd integer
        window: the type of window from 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'
            flat window will produce a moving average smoothing.

    output:
        the smoothed signal

    see also:

    numpy.hanning, numpy.hamming, numpy.bartlett, numpy.blackman, numpy.convolve
    scipy.signal.lfilter

    NOTE: length(output) != length(input), to correct this: return y[(window_len/2-1):-(window_len/2)] instead of just y.
    """

    if x.ndim != 1:
        raise (ValueError, "smooth only accepts 1 dimension arrays.")

    if x.size < window_len:
        raise (ValueError, "Input vector needs to be bigger than window size.")


    if window_len<3:
        return x


    if not window in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']:
        raise (ValueError, "Window is on of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'")


    s=np.r_[x[window_len-1:0:-1],x,x[-2:-window_len-1:-1]]
    #print(len(s))
    if window == 'flat': #moving average
        w=np.ones(window_len,'d')
    else:
        w=eval('np.'+window+'(window_len)')

    y=np.convolve(w/w.sum(),s,mode='valid')
    return y


def plotV3(V3_testing):
    """
    Input:
        * V3_testing -> [3 x num_predictions]
    Output:
        *Ploting the predictions
            0 -> right shift
            1 -> no shift
            2 -> right shift
    """
    fig, axs = plt.subplots(1, 1)

    for i in range(V3_testing.shape[0]):
        if (V3_testing[i][0] == 1):
            axs.plot(i, [2], 'bo')
        elif (V3_testing[i][1] == 1):
            axs.plot(i, [1], 'yo')
        else:
            axs.plot(i, [0], 'ro')

    axs.set_ylabel('Right;          No shift;           Left')
    axs.set_xlabel('Predictios')

    axs.set_ylim(-0.5, 2.5)


"""
Ploting the standard deviation of the predictions obtained for all possible number of BITS ON
"""
def plot_STD_STDerror(V3_testing_mean, V3_testing_std):
    meanOfMeans = V3_testing_mean.sum(0)/(V3_testing_mean != 0).sum(0) # Computing the mean of values different from zero
    fig, axs = plt.subplots(1, 3)

    #Left Shift
    axs[0].errorbar(range(1,10), V3_testing_mean[:,0], yerr=V3_testing_std[:,0], xerr=None, fmt='ob')
    axs[0].set_ylim(-0.1, 1.2)
    axs[0].hlines(meanOfMeans[0], 0, 10, colors='g', linestyles='dashed')
    axs[0].title.set_text('Std; Left shift')
    axs[0].set_ylabel('% of predictions')

    # No shift
    axs[1].errorbar(range(1,10), V3_testing_mean[:,1], yerr=V3_testing_std[:,1], xerr=None, fmt='ob')
    axs[1].set_ylim(-0.1, 1.2)
    axs[1].hlines(meanOfMeans[1], 0, 10, colors='g', linestyles='dashed')
    axs[1].title.set_text('Std; No shift')
    # axs.set_ylabel('% active when winning')
    axs[1].set_xlabel('Number of bits ON in the input examples')

    #Right shift
    axs[2].errorbar(range(1,10), V3_testing_mean[:,2], yerr=V3_testing_std[:,2], xerr=None, fmt='ob')
    axs[2].hlines(meanOfMeans[2], 0, 10, colors='g', linestyles='dashed')
    axs[2].set_ylim(-0.1, 1.2)
    axs[2].title.set_text('Std; Right shift')



def plot_meanAndSTD(mean, std):
    meanOfMeans = mean.sum(0)/(mean != 0).sum(0) # Computing the mean of values different from zero

    fig, axs = plt.subplots(1, 3)

    #Left Shift
    axs[0].errorbar(range(0,10), mean[:,0], yerr=std[:,0], xerr=None, fmt='ob')
    axs[0].set_ylim(-0.1, 1.2)
    axs[0].title.set_text('Std; Left shift')
    axs[0].set_ylabel('Num tSteps each population was winning')
    axs[0].hlines(meanOfMeans[0], 0, 10, colors='g', linestyles='dashed')
    axs[0].hlines(0.33, 0, 10, colors='y', linestyles='dashed')

    # No shift
    axs[1].errorbar(range(0,10), mean[:,1], yerr=std[:,1], xerr=None, fmt='ob')
    axs[1].set_ylim(-0.1, 1.2)
    axs[1].title.set_text('Std; No shift')
    # axs.set_ylabel('% active when winning')
    axs[1].set_xlabel('Number of bits ON in the samples used')
    axs[1].hlines(meanOfMeans[1], 0, 10, colors='g', linestyles='dashed')
    axs[1].hlines(0.33, 0, 10, colors='y', linestyles='dashed')


    #Right shift
    axs[2].errorbar(range(0,10), mean[:,2], yerr=std[:,2], xerr=None, fmt='ob')
    axs[2].set_ylim(-0.1, 1.2)
    axs[2].title.set_text('Std; Right shift')
    axs[2].hlines(meanOfMeans[2], 0, 10, colors='g', linestyles='dashed')
    axs[2].hlines(0.33, 0, 10, colors='y', linestyles='dashed')



def plot_meanAndSTD_activeNeurons(mean, std):
    # meanOfMeans = mean.sum(0)/(mean != 0).sum(0) # Computing the mean of values different from zero

    fig, axs = plt.subplots(1, 3)

    #Left Shift
    axs[0].errorbar(range(0,10), mean[:,0], yerr=std[:,0], xerr=None, fmt='ob')
    # axs[0].set_ylim(-0.1, 1.2)
    axs[0].title.set_text('Std; Left shift')
    axs[0].set_ylabel('Num tSteps each population was winning')
    # axs[0].hlines(meanOfMeans[0], 0, 10, colors='g', linestyles='dashed')
    # axs[0].hlines(0.33, 0, 10, colors='y', linestyles='dashed')

    # No shift
    axs[1].errorbar(range(0,10), mean[:,1], yerr=std[:,1], xerr=None, fmt='ob')
    # axs[1].set_ylim(-0.1, 1.2)
    axs[1].title.set_text('Std; No shift')
    # axs.set_ylabel('% active when winning')
    axs[1].set_xlabel('Number of bits ON in the samples used')
    # axs[1].hlines(meanOfMeans[1], 0, 10, colors='g', linestyles='dashed')
    # axs[1].hlines(0.33, 0, 10, colors='y', linestyles='dashed')


    #Right shift
    axs[2].errorbar(range(0,10), mean[:,2], yerr=std[:,2], xerr=None, fmt='ob')
    # axs[2].set_ylim(-0.1, 1.2)
    axs[2].title.set_text('Std; Right shift')
    # axs[2].hlines(meanOfMeans[2], 0, 10, colors='g', linestyles='dashed')
    # axs[2].hlines(0.33, 0, 10, colors='y', linestyles='dashed')



def plot_sumTC(tSteps, populationWinning, activeNeurons, num_tSteps_populationWin):
    """
    This fucntion works/uses a raster for a SINGLE ewxample
    """
    fig, axs2 = plt.subplots(2, 1)

    # Shows how many active neurons when the population was winning
    s1 = populationWinning[0] * activeNeurons[0]/100
    s2 = populationWinning[1] * activeNeurons[1]/100
    s3 = populationWinning[2] * activeNeurons[2]/100

    window_len=20 # Size of the smoothing windows
    window='hamming' # ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']

    s1_smooth = smooth(s1, window_len, window)
    s2_smooth = smooth(s2, window_len, window)
    s3_smooth = smooth(s3, window_len, window)

    t = np.arange(0, s1_smooth.shape[0], 1)
    axs2[0].set_xlim(0, tSteps)
    # axs.set_ylim(6, 10)

    axs2[0].plot(t, s1_smooth, 'b', label='Left')
    axs2[0].plot(t, s2_smooth, 'r', label='No shift')
    axs2[0].plot(t, s3_smooth, 'y', label='Right')

    axs2[0].set_ylabel('% active when winning')
    axs2[0].set_xlabel('Time steps')


    """ Ploting as a histogram the number of tSteps each population were winning """
    axs2[1].bar([1], [num_tSteps_populationWin[0]/tSteps], width=0.3, color = 'b')
    axs2[1].bar([2], [num_tSteps_populationWin[1]/tSteps], width=0.3, color = 'r')
    axs2[1].bar([3], [num_tSteps_populationWin[2]/tSteps], width=0.3, color = 'y')
    axs2[1].set_ylim(0, 0.5)

    shifts = ['Left','No shift','Right']
    plt.xticks([1, 2, 3], shifts)
    axs2[1].set_ylabel('% tSteps winning')

    plt.legend(shifts,loc=2)

    plt.show()
