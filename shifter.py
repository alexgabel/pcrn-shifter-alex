import numpy as np

import samplesGenerator as sg
import runNet as runNet
import tools as tools

import json
import math
from numpy.linalg import multi_dot
import matplotlib.pyplot as plt

################################## PCRN ######################################

def makeJEE_FromPatts(Patts):

    # Generate exci-exci connectivity matrix for P patterns of Nexci bits each

    Nexci = np.shape(Patts)[0]
    P = np.shape(Patts)[1]
    bitsPerPatt = Nexci/P
    JEE= np.zeros([Nexci,Nexci])

    JEE = np.zeros([Nexci,Nexci])

    for i in range(np.shape(Patts)[1]):
        outerProduct = np.outer(Patts[:,i], np.transpose(Patts[:,i]))
        JEE[i*int(bitsPerPatt):(i+1)*int(bitsPerPatt), i*int(bitsPerPatt):(i+1)*int(bitsPerPatt)] = outerProduct[i*int(bitsPerPatt):(i+1)*int(bitsPerPatt), i*int(bitsPerPatt):(i+1)*int(bitsPerPatt)]

    # Step function for binary patterns 0 or 1
    if np.shape(np.unique(Patts))[0] <= P:
        JEE=1*(JEE>0) # (RS'89, aka Willshaw..)

    return JEE

def makeW_simplePCRN(Jij,Ninhi,Jee,Jie,Kii,Kei):

    Nexci = Jij.shape[0]
    N = Nexci + Ninhi
    isExci = Nexci
    W = np.zeros((N,N))

    W[0:isExci, 0:isExci] += Jee*Jij
    W[isExci:, isExci:] = -Kii
    W[0:isExci, isExci:] = -Kei
    W[isExci:, 0:isExci] = Jie

    return W

def get_extended_W(W,hExt0=0.27,VLN=0.001,label=None):
    # noV3Num = totalNum_bitsNetwork - units.get("bits_V3") # leaving out the labels (top left W_extended w/o pops)
    # Nep = int(params.get("Nexci")/units.get("bits_V3")) # seperate Nexci into popualtions per V3 bit
    idxV3 = units.get("bits_V1") + units.get("bits_V2") # index of V3 in W
    hid_theta = units.get("hidden_units") + units.get("threshold")

    # thetha = np.array( [-1])

    """ Creating extended W """
    W_extended = np.zeros([W.shape[0]-units.get("bits_V3")+params.get("Nexci")+params.get("Ninhi"),
                           W.shape[0]-units.get("bits_V3")+params.get("Nexci")+params.get("Ninhi")])

    """ Setting the input of the output - And the feedback to the hidden units """
    for i in range(noV3Num, noV3Num+Nep):
        # Input to the output unit and using the threshold (bottom left of W_extended)
        W_extended[i,idxV3:idxV3+hid_theta] = W[idxV3,totalNum_bitsSamples:]*VLN
        W_extended[i+Nep,idxV3:idxV3+hid_theta] = W[idxV3+1,totalNum_bitsSamples:]*VLN
        W_extended[i+2*Nep,idxV3:idxV3+hid_theta] = W[idxV3+2,totalNum_bitsSamples:]*VLN

    for j in range(idxV3,idxV3+units.get("hidden_units")):
        V3_left = W_extended[noV3Num, j]/VLN
        V3_stay = W_extended[noV3Num+Nep, j]/VLN
        V3_right = W_extended[noV3Num+2*Nep, j]/VLN

        # if label == 0:
        #     feedBackToHiddenUnits_left = 1/(Nep*((hExt0 + V3_left)/2))
        #     feedBackToHiddenUnits_stay = 1/(Nep*(V3_stay/2))
        #     feedBackToHiddenUnits_right = 1/(Nep*(V3_right/2))
        # elif label == 1:
        #     feedBackToHiddenUnits_left = 1/(Nep*(V3_left/2))
        #     feedBackToHiddenUnits_stay = 1/(Nep*((hExt0 + V3_stay)/2))
        #     feedBackToHiddenUnits_right = 1/(Nep*(V3_right/2))
        # elif label == 2:
        #     feedBackToHiddenUnits_left = 1/(Nep*(V3_left/2))
        #     feedBackToHiddenUnits_stay = 1/(Nep*(V3_stay/2))
        #     feedBackToHiddenUnits_right = 1/(Nep*((hExt0 + V3_right)/2))
        # else:
        #     feedBackToHiddenUnits_left = 1/(Nep*((hExt0 + V3_left)/2))
        #     feedBackToHiddenUnits_stay = 1/(Nep*((hExt0 + V3_stay)/2))
        #     feedBackToHiddenUnits_right = 1/(Nep*((hExt0 + V3_right)/2))

        feedBackToHiddenUnits_left = V3_left/(Nep*(hExt0/2))
        feedBackToHiddenUnits_stay = V3_stay/(Nep*(hExt0/2))
        feedBackToHiddenUnits_right = V3_right/(Nep*(hExt0/2))

        W_extended[j, noV3Num:noV3Num+Nep] = np.repeat(feedBackToHiddenUnits_left, Nep)
        W_extended[j, noV3Num+Nep:noV3Num+2*Nep] = np.repeat(feedBackToHiddenUnits_stay, Nep)
        W_extended[j, noV3Num+2*Nep:noV3Num+3*Nep] = np.repeat(feedBackToHiddenUnits_right, Nep)

    """ Eliminating the V3 in the shifter connectivity matrix """
    W = np.delete(W, [idxV3,idxV3+1,idxV3+2], 0) # Deleting the rows that belong to V3
    W = np.delete(W, [idxV3,idxV3+1,idxV3+2], 1) # Deleting the colums that belong to V3

    """ Setting W from shifter without original V3 into the extended W """
    W_extended[0:noV3Num,0:noV3Num] = W

    """ Creating and setting W from PCRN """
    Jee = params.get("Jee")
    Jie = params.get("Jie")
    Kii = params.get("Kii")
    Kei = params.get("Kei")
    Nexci = params.get("Nexci") # Number of exitatory neurons
    Ninhi = params.get("Ninhi") # Number of inhibitory neurons
    NinhiBand = params.get("NinhiBand")
    N = Nexci+Ninhi; # Total number of neurons
    P = params.get("Patterns")  # Number of atractor states

    bitsPerPatt = np.floor(Nexci/P) # Number of neurons per population (size of the rectangle)
    Patts = np.zeros([Nexci,P])

    for mu in range(P): # Completation of the patterns of activity
        Patts[mu*int(bitsPerPatt):(mu+1)*int(bitsPerPatt), mu] = 1

    Jij = makeJEE_FromPatts(Patts)

    W_pcrn = makeW_simplePCRN(Jij,Ninhi,Jee,Jie,Kii,Kei)

    W_extended[noV3Num:,noV3Num:] = W_pcrn

    return W_extended

def reduced(V):
    ''' reduces V in size by taking mean of activity over pop.s (and swaps columns)'''
    idxV3 = units.get("bits_V1") + units.get("bits_V2")
    V_red = np.zeros((V.shape[0],totalNum_bitsNetwork))

    V_red[:,:idxV3] = V[:,:idxV3]
    V_red[:,idxV3] = np.mean(V[:,noV3Num:noV3Num+Nep],axis=1)
    V_red[:,idxV3+1] = np.mean(V[:,noV3Num+Nep:noV3Num+2*Nep],axis=1)
    V_red[:,idxV3+2] = np.mean(V[:,noV3Num+2*Nep:noV3Num+3*Nep],axis=1)
    V_red[:,idxV3+3:] = V[:,idxV3:noV3Num]
    return V_red

def show(elements):
    plt.figure(figsize=(20,20))
    plt.imshow(elements)
    plt.colorbar()
    plt.grid(b=None)
    plt.show(block=False)

def show_raster(elements, transpose=True, big=True,timecourse=True,window=5,start=51):
    if big:
        plt.figure(figsize=(20,10))
    else:
        plt.figure()
    if transpose:
        plt.imshow(elements.transpose())
    else:
        plt.axis('off')
        plt.imshow(elements)
    plt.colorbar()
    plt.grid(b=None)
    plt.show(block=False)
    if timecourse:
        Vt = elements
        # Vt = Vt[:,:]
        plt.figure(figsize=(15,3))
        plt.plot(np.convolve(np.sum(Vt[:,start:start+100],axis=1), np.ones(window)/window))
        plt.plot(np.convolve(np.sum(Vt[:,start+100:start+200],axis=1), np.ones(window)/window))
        plt.plot(np.convolve(np.sum(Vt[:,start+200:start+300],axis=1), np.ones(window)/window))
        plt.plot(np.convolve(np.sum(Vt[:,start+300:],axis=1), np.ones(window)/window))
        plt.show()

##########################################################################

params = {
    "sweeps" : 1000, # Number of sweeps used during the learning
    "num_samples" : 20, # Default number of examples per sweep
    "threshold" : -1, # Value of the threshold
    "tSteps": 1000, # Number of time steps in which we will "evaluate" the populations
    "Patterns": 3, # Number of patterns (number of populations)
    "Jee": 1,
    "Jie": 1,
    "Kii": 1,
    "Kei": 3,
    "Temperature": 1.4, # Original temperature 1.3
    "Nexci": 300, #Number of exitatory neurons (100 per population)
    "Ninhi": 300, # Number of inhibitory neurons (100 per population) Orig: 300
    "NinhiBand": 50, #75 # How many inhi units to include in raster
    "hExt0": 0.27, # Externat stimulation (base stimulation used) # Original hExt0 = 0.31
    "VLN": 0.001, # Very large number to reduce the weight of the learning (shifter network) (Orig: 0.001)
}

# ================================================================
# Setting main characteristics of the learning
# ================================================================

# Setting the number of bits for the samples and network
units = { # Diccionary with the saze of all the parameters
    "bits_V1": 10,
    "bits_V2": 10,
    "bits_V3": 3,
    "hidden_units": 30,
    "threshold": 1
}

# Size of the components (bits per input vector)
totalNum_bitsSamples = units.get("bits_V1") + units.get("bits_V2") + units.get("bits_V3")
totalNum_bitsSamplesAndHiddenUnits = totalNum_bitsSamples + units.get("hidden_units")
totalNum_bitsNetwork =totalNum_bitsSamplesAndHiddenUnits + units.get("threshold")
noV3Num = totalNum_bitsNetwork - units.get("bits_V3")
Nep = int(params.get("Nexci")/units.get("bits_V3"))
# ================================================================
# Function required during de learning
# ================================================================

def settings(V1, V2, V3, hidden_units, threshold):
    """
    This functino is used to build the connectivity matrix of the shifter network

    Input:
        * V1, V2, V3: are the input examples. The important aspect is the shape of the one-dimentional vecotrs
        * hidden units: number of hidden units used to capturate the high order relationships
        * threhold: shape of the threshold

    Output:
        * h: one-dimensional vector of zeros with the size of the number of hidden units
        * threshold: value of the threshold
        * W: connectivity matrix (without knowledge - initialization)
        * archM: binary matrix indicating which values of the conectivity matrix can be changed
        * pij_plus: vector used to encode the knowledge from the possitive phase
        * pij_minus: vector used to encode the knowledge from the negative phase
    """
    # totalNum_bitsSamples = units.get("bits_V1") + units.get("bits_V2") + units.get("bits_V3")
    # totalNum_bitsSamplesAndHiddenUnits = totalNum_bitsSamples + units.get("hidden_units")
    # totalNum_bitsNetwork =totalNum_bitsSamplesAndHiddenUnits + units.get("threshold")
    # Hidden units
    h = np.zeros(hidden_units)

    # Weights -connectivity metrix
    W = np.zeros([V1.shape[1] + V2.shape[1] + V3.shape[1] + h.shape[0] + units.get("threshold"),
                  V1.shape[1] + V2.shape[1] + V3.shape[1] + h.shape[0] + units.get("threshold")])

    # Setting the value threshold
    W[:-1,-1] = threshold

    # Random intialization for the weights
    W[0:totalNum_bitsSamples,
      totalNum_bitsSamples:totalNum_bitsSamplesAndHiddenUnits] = np.random.uniform(-1,
                                                                                    1,
                                                                                    size=[totalNum_bitsSamples,hidden_units])

    W = W + np.transpose(W)

    # Defining the architecture mextrix
    archM = np.zeros([V1.shape[1] + V2.shape[1] + V3.shape[1] + h.shape[0] + np.abs(threshold),
                     V1.shape[1] + V2.shape[1] + V3.shape[1] + h.shape[0] + np.abs(threshold)])

    archM[:-1,-1] = 1
    archM[0:totalNum_bitsSamples, totalNum_bitsSamples:totalNum_bitsSamplesAndHiddenUnits] = 1
    archM = archM + np.transpose(archM)

    # Initalization oj Pij+ and Pij-
    pij_plus = np.zeros([ totalNum_bitsNetwork , totalNum_bitsNetwork ])
    pij_minus = np.zeros([ totalNum_bitsNetwork , totalNum_bitsNetwork ])

    return h, threshold, W, archM, pij_plus, pij_minus

def coOcurrances(V, iterations):
    """
    This function is used to compute the coocurrrances (kronecker product or outer product)
    given the state vector (V) of the network.

    Input:
        * V: state vector of the network
        * iterations: number of iterations along the state vector has been collected
    """
    pij_temp = 0
    for i in range(iterations):
        kronecker_tensor_product = np.outer(V[i], np.transpose(V[i]))
        pij_temp  += kronecker_tensor_product.reshape(( totalNum_bitsNetwork , totalNum_bitsNetwork ))

    return pij_temp

# ================================================================
# Learning algorithm of the shifter network (Boltzmann machine)
# ================================================================

def shifter_train(num_samples=20,sweeps=1000, verbose=True):

    T = [40, 35, 30, 25, 20, 15, 12, 10] # Annelaing temperatures
    pos_lerning_rate = 5
    neg_leranig_rate = 0.0005

    # Creating the samples and all the elements for the training
    V1, V2, V3 = sg.samples(units, num_samples)
    h, threshold, W, archM, pij_plus, pij_minus = settings(V1, V2, V3, units.get("hidden_units"), params.get("threshold"))

    for s in range(sweeps):

        '''PHASE+'''
        for i in range(num_samples):
            V = np.transpose(np.concatenate((V1[i], V2[i], V3[i], h, [threshold])))

            '''Annealing'''
            for t in T:
                V = runNet.runNet(W, V, t, [totalNum_bitsSamples, totalNum_bitsSamplesAndHiddenUnits], 2, 0)[-1]

            '''Thermal equilibrium '''
            V_temp = runNet.runNet(W, V, 10, [totalNum_bitsSamples, totalNum_bitsSamplesAndHiddenUnits], 10, 0)
            pij_plus += coOcurrances(V_temp, 10)

        '''Averga of Pij+'''
        pij_plus = pij_plus/(num_samples*10)

        '''PHASE-'''
        for i in range(num_samples):
            V = np.transpose(np.concatenate((V1[i], V2[i], V3[i], h, [threshold])))

            '''Annealing'''
            for t in T:
                V = runNet.runNet(W, V, t, [0, totalNum_bitsSamplesAndHiddenUnits], 2, 0)[-1]

            '''Thermal equilibrium '''
            V_temp = runNet.runNet(W, V, 10, [0, totalNum_bitsSamplesAndHiddenUnits], 10, 0)

            pij_minus += coOcurrances(V_temp, 10)

        '''Averga of Pij-'''
        pij_minus = pij_minus/(num_samples*10)

        # Updating the weights matrix -> W = W + 5Variation(W) - Decay
        W += pos_lerning_rate*archM*(pij_plus-pij_minus)*(1 - neg_leranig_rate)
        # Re-seting the probabilities
        pij_plus = np.zeros([ totalNum_bitsNetwork , totalNum_bitsNetwork ])
        pij_minus = np.zeros([ totalNum_bitsNetwork , totalNum_bitsNetwork ])
        # Uncomment to use 20 different samples per sweep
        V1, V2, V3 = sg.samples(units, num_samples)

        if verbose and s%100 == 0:
            print('Sweep: ', str(s), '/', str(sweeps))
            print(s//100*'='+'>'+(sweeps-s)//100*'.'+'|')

    # Plotting
    plt.style.use('ggplot')
    fig, axes = plt.subplots(nrows=6, ncols=5)

    _min, _max = np.min(W), np.max(W)

    i = 1
    for ax in axes.flat:
        plot = np.zeros([4,10])
        # Plot threshold
        plot[0,0] = W[22+i,-1]
        # Plot V3
        plot[0, 3:6] = W[22+i, 20:23]
        # Plot V1
        plot[3,:] = W[22+i, 0:10]
        #Plot V2
        plot[2, :] = W[22+i, 10:20]
        ax.set_axis_off()
        im = ax.imshow(plot, cmap='gray', vmin=_min, vmax=_max)
        i += 1

    fig.subplots_adjust(right=0.95)
    cbar_ax = fig.add_axes([1, 0.15, 0.05, 0.7])
    fig.colorbar(im, cax=cbar_ax)

    plt.show()

    return W

def shifter_train_plots(num_samples=20,sweeps=1000, verbose=True, show_every=100):
    W_stored = np.zeros((sweeps,totalNum_bitsNetwork,totalNum_bitsNetwork))
    T = [40, 35, 30, 25, 20, 15, 12, 10] # Annelaing temperatures
    pos_lerning_rate = 5
    neg_leranig_rate = 0.0005
    # Creating the samples and all the elements for the training
    V1, V2, V3 = sg.samples(units, num_samples) # Nx10, Nx10, Nx3; i.e. N samples
    h, threshold, W, archM, pij_plus, pij_minus = settings(V1, V2, V3, units.get("hidden_units"), params.get("threshold"))

    for s in range(sweeps):
        for i in range(num_samples): # Phase +
            V = np.transpose(np.concatenate((V1[i], V2[i], V3[i], h, [threshold]))) # 1d numpy array: size (totalNum_bitsNetwork,)
            # Annealing
            for t in T: # two iterations at each temperature in T, w. clamped visible units
                V = runNet.runNet(W, V, t, [totalNum_bitsSamples, totalNum_bitsSamplesAndHiddenUnits], 2, 0)[-1]
            # Thermal equilibrium (set of 10 iterations of V @ temp 10)
            V_temp = runNet.runNet(W, V, 10, [totalNum_bitsSamples, totalNum_bitsSamplesAndHiddenUnits], 10, 0) # 10 x totalNum_bitsNetwork
            pij_plus += coOcurrances(V_temp, 10)
        # Average of Pij+
        pij_plus = pij_plus/(num_samples*10)

        for i in range(num_samples): # Phase -
            V = np.transpose(np.concatenate((V1[i], V2[i], V3[i], h, [threshold])))
            # Annealing
            for t in T:
                V = runNet.runNet(W, V, t, [0, totalNum_bitsSamplesAndHiddenUnits], 2, 0)[-1]
            # Thermal equilibrium
            V_temp = runNet.runNet(W, V, 10, [0, totalNum_bitsSamplesAndHiddenUnits], 10, 0)
            pij_minus += coOcurrances(V_temp, 10)
        # Average of Pij-
        pij_minus = pij_minus/(num_samples*10)

        # Updating the weights matrix -> W = W + 5Variation(W) - Decay
        #W += pos_lerning_rate*archM*(pij_plus-pij_minus)*(1 - neg_leranig_rate)

        W += pos_lerning_rate*archM*(pij_plus-pij_minus)
        W = W*(1 - neg_leranig_rate) # corrected Update rule!

        W_stored[s] = W
        # Re-seting the probabilities
        pij_plus = np.zeros([ totalNum_bitsNetwork , totalNum_bitsNetwork ])
        pij_minus = np.zeros([ totalNum_bitsNetwork , totalNum_bitsNetwork ])
        # Uncomment to use 20 different samples per sweep
        V1, V2, V3 = sg.samples(units, num_samples)

        if verbose and s%show_every == 0:
            print('Sweep: ', str(s), '/', str(sweeps))
            print(s//show_every*'='+'>'+(sweeps-s)//show_every*'.'+'|')

            plot_W(W,s)

    return W_stored

def plot_W(W,s=None,save=False,bits=10, suppress_show=False):
    plt.style.use('ggplot')

    if bits==10:
        fig, axes = plt.subplots(nrows=6, ncols=5)
        if s:
            fig.suptitle('Sweep: '+str(s))
        _min, _max = np.min(W), np.max(W)

        i = 1
        for ax in axes.flat:
            plot = np.zeros([4,10])
            # Plot threshold
            plot[0,0] = W[22+i,-1]
            # Plot V3
            plot[0, 3:6] = W[22+i, 20:23]
            # Plot V1
            plot[3,:] = W[22+i, 0:10]
            #Plot V2
            plot[2, :] = W[22+i, 10:20]
            ax.set_axis_off()
            im = ax.imshow(plot, cmap='gray', vmin=_min, vmax=_max)
            i += 1
    elif bits==8:
        fig, axes = plt.subplots(nrows=4, ncols=6)
        if s:
            fig.suptitle('Sweep: '+str(s))
        _min, _max = np.min(W), np.max(W)

        i = 0
        for ax in axes.flat:
            plot = np.zeros([4,bits])
            # Plot threshold
            plot[0,0] = W[totalNum_bitsSamples+i,-1]
            # Plot V3
            plot[0, 3:6] = W[totalNum_bitsSamples+i, totalNum_bitsSamples-3:totalNum_bitsSamples]
            # Plot V1
            plot[3,:] = W[totalNum_bitsSamples+i, 0:bits]
            #Plot V2
            plot[2, :] = W[totalNum_bitsSamples+i, bits:2*bits]
            ax.set_axis_off()
            im = ax.imshow(plot, cmap='gray', vmin=_min, vmax=_max)
            i += 1

    fig.subplots_adjust(right=0.95)
    cbar_ax = fig.add_axes([1, 0.15, 0.05, 0.7])
    fig.colorbar(im, cax=cbar_ax)

    if save:
        plt.savefig('connW_saved/'+str(s)+'.png',bbox_inches='tight')

    if not suppress_show:
        plt.show()

def combine_shifter_PCRN(W, VLN=0.001, num_samples=10,bitsOn=6, verbose=True, hExt0=0.27, ambiguous=False, shifts=[0,2],tSteps=1000,set_seed=None):
    # ================================================================
    # Combining the connectivity matrices (shifter nwtwork and PCRN)
    # ================================================================
    if set_seed:
        np.random.seed(set_seed)
    bitsOn_sample = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    seeds = np.random.randint(0, 10000, len(bitsOn_sample)+3) # Seeds used to generate the input examples
    if ambiguous:
        [V1, V2, V3] = sg.maximalAambiguous_shiftFixed(seeds[bitsOn], units, num_samples, bitsOn,shifts=[0,2])
    else:
        [V1, V2, V3] = sg.unambiguous(seeds[bitsOn], units, num_samples, bitsOn)

    thetha = np.array( [-1])

    """ Creating extended W """
    W_extended = np.zeros([W.shape[0]-3+params.get("Nexci")+params.get("Ninhi"), W.shape[0]-3+params.get("Nexci")+params.get("Ninhi")])

    """ Setting the input of the output - And the feedback to the hidden units """
    for i in range(51,51+100):
        # Input to the output unit and using the threshold
        W_extended[i, 20:20+31] = W[20,23:] * VLN
        W_extended[i+100, 20:20+31] = W[21,23:] * VLN
        W_extended[i+200, 20:20+31] = W[22,23:] * VLN

    for j in range(20,20+30):
        V3_left = W_extended[51, j]
        V3_stay = W_extended[151, j]
        V3_right = W_extended[251, j]

        feedBackToHiddenUnits_left = 1/(100*((hExt0 + V3_left)/2))
        feedBackToHiddenUnits_stay = 1/(100*((hExt0 + V3_stay)/2))
        feedBackToHiddenUnits_right = 1/(100*((hExt0 + V3_right)/2))

        W_extended[j, 51:51+100] = np.repeat(feedBackToHiddenUnits_left, 100)
        W_extended[j, 51+100:51+200] = np.repeat(feedBackToHiddenUnits_stay, 100)
        W_extended[j, 51+200:51+300] = np.repeat(feedBackToHiddenUnits_right, 100)

    """ Eliminating the V3 in the shifter connectivity matrix """
    W = np.delete(W, [20,21,22], 0) # Deleting the rows that belong to V3
    W = np.delete(W, [20,21,22], 1) # Deleting the colums that belong to V3

    """ Setting W from shifter without original V3 into the extended W """
    W_extended[0:54-3,0:54-3] = W

    """ Creating and setting W from PCRN """
    Jee = params.get("Jee")
    Jie = params.get("Jie")
    Kii = params.get("Kii")
    Kei = params.get("Kei")
    Nexci = params.get("Nexci") # Number of exitatory neurons
    Ninhi = params.get("Ninhi") # Number of inhibitory neurons
    NinhiBand = params.get("NinhiBand")
    N = Nexci+Ninhi; # Total number of neurons
    P = params.get("Patterns")  # Number of atractor states

    bitsPerPatt = np.floor(Nexci/P) # Number of neurons per population (size of the rectangle)
    Patts = np.zeros([Nexci,P])

    for mu in range(P): # Completation of the patterns of activity
        Patts[mu*int(bitsPerPatt):(mu+1)*int(bitsPerPatt), mu] = 1

    Jij = makeJEE_FromPatts(Patts)

    W_pcrn = makeW_simplePCRN(Jij,Ninhi,Jee,Jie,Kii,Kei)

    W_extended[54-3:,54-3:] = W_pcrn

    """ Computing and setting delta_hExt """
    #hExt0 = params.get("hExt0") # Externat stimulation (base)

    hExt=np.zeros((W_extended.shape[0],))
    hExt[51:51+Nexci] = hExt0

    # ================================================================
    # Predicting the different input examples
    # ================================================================

    threshold = -1
    temperature = params.get("Temperature")

    numBitsV1V2 = units.get("bits_V1") + units.get("bits_V2")
    totalNum_bitsNetwork =numBitsV1V2 + units.get('hidden_units') + units.get("threshold") + Nexci + Ninhi
    unClamped_indexes = [numBitsV1V2, totalNum_bitsNetwork]

    timeSteps = tSteps

    predictions = np.zeros([num_samples, 3])
    activeNeurons_testing = np.zeros([num_samples, 3])
    populationWinning_testing = np.zeros([num_samples, 3])
    proportionActive_testing = np.zeros([num_samples, 3])

    for i in range(num_samples):
        if verbose:
            print('Computing results input sample: ' + str(i+1))

        V = np.transpose(np.concatenate((V1[i], V2[i],
        #                                 V_afterAnnealing[23:-1], # Use it if you want to add info from the shifter network (e.g. part of annealing)
                                        np.zeros(units.get('hidden_units')),
                                        [threshold],
                                        np.zeros(Nexci+Ninhi),
                                        )))

        Vt = runNet.runNet(W_extended, V, temperature, unClamped_indexes, timeSteps, hExt*np.mean(np.sum(Jij,1)))

        # Uncomment the next lines to save the rasters from all the examples
        # np.save(root + newPath +'/Vt_'
        #     + str(shift) + type_shifterSamples + '_'
        #     + str(num_samples)+'samps_'
        #     + str(bitsOn)+'bitsOn_'
        #     + str(timeSteps)+'tSteps_'
        #     + str(Nexci+Ninhi)+'N_'
        #     +str(VLN)+'VLN_'
        #     + 'examples' + str(i),
        #     Vt.astype(np.int8))


        """ Printing aditional/useful information """
        [activeNeurons, populationWinning, num_tSteps_populationWin, proportionActive] = tools.meanActivity_winning(
                                                                                                    tSteps,
                                                                                                    np.transpose(Vt[:, 50:350]),
                                                                                                    (params.get('Nexci')/params.get('Patterns')))

        predictions[i] = num_tSteps_populationWin
        proportionActive_testing[i] = proportionActive

    return V1, V2, V3, Vt, activeNeurons, populationWinning, predictions, proportionActive_testing
    # proportionActive_testing

def learn_shifter_PCRN(num_samples=20,sweeps=1000, tSteps=400, verbose=True,
                       VLN=0.001, hExt0=0.27, set_seed=42, show_plots=False,
                       show_every=100, bits=10, show_anything=True, quiet_save=True, keep_hist=False):
    ''' Learns the weights using a PCRN (in development) '''
    pos_lerning_rate = 5
    neg_leranig_rate = 0.0005

    Jee = params.get("Jee")
    Jie = params.get("Jie")
    Kii = params.get("Kii")
    Kei = params.get("Kei")
    Nexci = params.get("Nexci") # Number of exitatory neurons
    Ninhi = params.get("Ninhi") # Number of inhibitory neurons
    NinhiBand = params.get("NinhiBand")
    N = Nexci+Ninhi; # Total number of neurons
    P = params.get("Patterns")  # Number of atractor states

    if set_seed:
        np.random.seed(set_seed)

    global units
    units["bits_V1"] = bits
    units["bits_V2"] = bits
    units["hidden_units"] = bits*units["bits_V3"]

    global totalNum_bitsSamples
    global totalNum_bitsSamplesAndHiddenUnits
    global totalNum_bitsNetwork
    global noV3Num
    global Nep
    totalNum_bitsSamples = units.get("bits_V1") + units.get("bits_V2") + units.get("bits_V3")
    totalNum_bitsSamplesAndHiddenUnits = totalNum_bitsSamples + units.get("hidden_units")
    totalNum_bitsNetwork =totalNum_bitsSamplesAndHiddenUnits + units.get("threshold")
    noV3Num = totalNum_bitsNetwork - units.get("bits_V3")
    Nep = int(params.get("Nexci")/units.get("bits_V3"))

    if keep_hist:
        W_stored = np.zeros((sweeps,totalNum_bitsNetwork,totalNum_bitsNetwork))

    # Creating the samples and all the elements for the training
    V1, V2, V3 = sg.samples(units, num_samples)
    h, threshold, W, archM, pij_plus, pij_minus = settings(V1, V2, V3, units.get("hidden_units"), params.get("threshold"))

    #####################################################
    ######### EXTENDED W SET-UP #########################
    #####################################################
    W_extended = get_extended_W(W, hExt0, VLN)

    """ Computing and setting delta_hExt """
    hExt=np.zeros((4,W_extended.shape[0]))
    ########### CLAMPED BASE STIMULATIONS #############
    hExt[0,noV3Num:noV3Num+Nep] = hExt0
    hExt[1,noV3Num+Nep:noV3Num+2*Nep] = hExt0
    hExt[2,noV3Num+2*Nep:noV3Num+3*Nep] = hExt0
    hExt[3,noV3Num:noV3Num+Nexci] = hExt0 # UNCLAPMED BASE STIMULATION ############

    ######## Setting Parameters ###########
    threshold = -1
    t = params.get("Temperature")

    numBitsV1V2 = units.get("bits_V1") + units.get("bits_V2")
    totalNum_Neurons =numBitsV1V2 + units.get('hidden_units') + units.get("threshold") + Nexci + Ninhi
    unClamped_indexes = [numBitsV1V2, totalNum_Neurons-1] # removed one to clamp threshold

    ############################################
    ############ TRAINING ######################
    ############################################

    for s in range(sweeps):

        '''PHASE+'''
        for i in range(num_samples):
            V = np.transpose(np.concatenate((V1[i], V2[i], h, [threshold], np.zeros(Nexci+Ninhi))))
            label = np.argmax(V3[i])
            #W_extended_temp = get_extended_W(W,hExt0, VLN,label=label)
            '''PCRN'''
            V_temp = runNet.runNet_flex(W_extended, V, t,
                    unClamped_indexes,
                    tSteps, hExt[label]*Nep, bits, Ninhi=Ninhi)
            if show_plots and s%show_every==0:
                show_raster(V_temp)
            # Change value of populations (treat whole pop as 1 unit)
            V_reduced = reduced(V_temp)
            pij_plus += coOcurrances(V_reduced, tSteps)

        '''Averga of Pij+'''
        pij_plus = pij_plus/(num_samples*tSteps)

        '''PHASE-'''
        for i in range(num_samples):
            V = np.transpose(np.concatenate((V1[i], V2[i], h, [threshold], np.zeros(Nexci+Ninhi))))

            '''PCRN'''
            V_temp = runNet.runNet_flex(W_extended, V, t,
                    [0,totalNum_Neurons-1], # removed one to clamp threshold
                    tSteps, hExt[3]*Nep,bits, Ninhi=Ninhi)
            if show_plots and s%show_every==0:
                show_raster(V_temp)
            # Change value of populations (treat whole pop as 1 unit)
            V_reduced = reduced(V_temp)
            pij_minus += coOcurrances(V_reduced, tSteps)

        '''Averga of Pij-'''
        pij_minus = pij_minus/(num_samples*tSteps)

        # Updating the weights matrix -> W = W + 5Variation(W) - Decay (wrong)
        W += pos_lerning_rate*archM*(pij_plus-pij_minus)
        W = W*(1 - neg_leranig_rate) # corrected Update rule!

        if keep_hist:
            W_stored[s] = W

        if show_anything and s%show_every == 0:
            plot_W(W,bits=bits)
        # show(W)
        W_extended = get_extended_W(W,hExt0,VLN)
        # Re-seting the probabilities
        pij_plus = np.zeros([ totalNum_bitsNetwork , totalNum_bitsNetwork ])
        pij_minus = np.zeros([ totalNum_bitsNetwork , totalNum_bitsNetwork ])
        # Uncomment to use 20 different samples per sweep
        V1, V2, V3 = sg.samples(units, num_samples)

        if verbose and s%show_every == 0:
            print('Sweep: ', str(s), '/', str(sweeps))
            print(s//show_every*'='+'>'+(sweeps-s)//show_every*'.'+'|')

    plot_W(W,s=str(num_samples)+'_'+str(tSteps)+'_'+str(sweeps),
           save=quiet_save,bits=bits,suppress_show=quiet_save) # Saves image of W

    if keep_hist:
        return W_stored
    else:
        return W
