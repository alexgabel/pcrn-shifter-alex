import numpy as np
import math
from typing import List, Set, Union, Dict, Tuple

import matplotlib.pyplot as plt
from scipy.linalg import block_diag


class PCRN:
    def __init__(self,
                 P: int = None,
                 Nep: Union[int,List[int]] = None,
                 Ni: int = None,
                 Jee: float = 1.,
                 Jie: float = 1.,
                 Kii: float = 1.,
                 Kei: float = 3.
                 ):
        """
        :param P: Number of excitatory sub-populations (automatically set if
        Nep is a list of integers).
        :param Nep: [[A list containing the number of neurons per subpopulation.]] -> Not yet implemented!
        If an integer is passed, it is the number of excitatory neurons per
        subpopulation. For the latter, P needs to be specified.
        :param Ni: Number of inhibitary neurons.
        --- Implement list Nep (different population sizes)? ---

        :param Jee: Intraexcitatory connection
        :param Jie: Connection between excitatory and inhibitor neurons
        :param Kii: Intrainhibitory connection
        :param Kei: Connection from inhibitor to excitatory neurons
        """
        self.Nep = Nep
        self.Ni = Ni
        self.P = P

        ### Additional neural parameters ###
        self.Ne = self.P*self.Nep # Total number of excitatory neurons
        self.Ntot = self.Ni + self.Ne # Grand total number of neurons in PCRN
        # inhibitor to excitatory-per-subpopulation ratio (r_I|E):
        self.r = self.Ni / self.Nep

        # Set connectivity parameters:
        self.Jee = Jee
        self.Jie = Jie
        self.Kii = Kii
        self.Kei = Kei
        self.K = self.Kei

        # Create block diagonal matrix J for excitatory neurons (J^EE)
        self.J = self.makeJ()

        # Initialize PCRN connectivity matrix W and augmented state vector V
        self.W = self.makeW()
        self.V = np.zeros(self.Ntot)

    def makeJ(self) -> np.ndarray:
        """
        Prepares J matrix by forming a block diagonal matrix. The number of
        blocks is determined by the number of subpopulations P. The size of
        the blocks is equal to the number of neurons in each subpopulation.

        :return: array of block diagonal matrix J of shape (Ne x Ne)
        """
        populations = []
        for i in range(self.P):
            populations.append(np.ones((self.Nep,self.Nep)))

        J = block_diag(*populations)

        return J

    def makeW(self) -> np.ndarray:
        """
        Forms the PCRN's full connectivity matrix from excitatory matrix J.

        :return: array of shape (Ntot x Ntot) representing the full connectivity
        matrix W given intraexcitatory connectivity J and all four connectivity
        parameters of the PCRN
        """
        Ne = self.Ne
        W = np.zeros((self.Ntot,self.Ntot))

        W[0:Ne, 0:Ne] += self.Jee*self.J
        W[Ne:, Ne:] = -self.Kii
        W[0:Ne, Ne:] = -self.Kei
        W[Ne:, 0:Ne] = self.Jie

        return W

    def staircase_hExt(self, *hExt) -> np.ndarray:
        """
        Sets the external signal to the first i subpopulations, where i is the
        number of inputs.
        :param hExt: List of external signal amplitutes per subpopulation
        :return: Vector array of external amplitudes per neuron, size (Ntot)
        """
        hx = np.zeros(self.Ntot)
        N = self.Nep

        for i in range(len(hExt)):
            hx[i*N:(i+1)*N] = hExt[i]*np.ones(N)

        return hx

    def run(self,
            steps: int,
            hExt: Union[float,List[float]],
            T: float,
            h_normalized: bool = True,
            h_per_population: bool = True
            ) -> np.ndarray:
        """
        Does calculation for V for t steps
        :param steps:
        :param hExt: hExt per population (if not normalized, h_EXT)
        :param T:
        :param h_normalized:
        :return: array of state vectors over time, shape (Ntot x steps)
        """
        N = self.Ntot
        W = self.W
        V0 = self.V

        if not h_per_population:
            h_EXT = hExt

        if isinstance(hExt,list):
            # Assert len(list_hExt) == self.P
            hExt = self.staircase_hExt(*hExt)
        else:
            list_hExt = [hExt for i in range(self.P)]
            hExt = self.staircase_hExt(*list_hExt)

        # Scale hExt if normalized (i.e., h_ext = h_EXT / N_EP)
        if h_normalized:
            h_EXT = hExt * self.Nep
        else:
            h_EXT = hExt

        if T != 0:
            beta = 1 / T
        else:
            beta = 0

        V_temporal = np.zeros((N,steps))

        k_stream = np.random.randint(0, N, N*steps)
        pk_toCompare = np.random.uniform(0, 1, N*steps)
        i_k = 0
        i_pk = 0

        h = np.dot(W,V0) + h_EXT

        for i in range(steps):
            for probe in range(N):
                k = k_stream[i_k]

                if np.log(1/pk_toCompare[i_pk]-1) > -beta*h[k]:
                    new_Vk = 1
                else:
                    new_Vk = 0

                deltaVk = new_Vk - V0[k]
                if deltaVk != 0:
                    h += deltaVk*W[:,k]
                    V0[k] = new_Vk

                i_k += 1
                i_pk += 1

            V_temporal[:,i] = V0

        self.Vt = V_temporal

        return V_temporal

    def __call__(self,
                 steps: int,
                 hExt: Union[float,List[float]],
                 T: float,
                 h_normalized: bool = True,
                 h_per_population: bool = True
                 ) -> np.ndarray:

        return self.run(steps, hExt, T, h_normalized, h_per_population)

    def show_W(self):
        plt.matshow(self.W)
        #plt.colorbar()
        plt.show()

    def plot(self, show_mean=False, window=10):
        output = self.Vt

        # Calculate and plot output:
        fig, (ax1, ax2) = plt.subplots(2,1, sharex=True, figsize=(15,8))


        t = self.Vt.shape[1]
        N = self.Nep
        P = self.P
        Ntot = self.Ne

        # Collect all output data in dict for plotting
        data_r = {}
        for n in range(P):
            r_n = []
            label = n
            for i in range(t):
                r_n.append(np.sum(output[n*N:(n+1)*N,i]))
            data_r[label] = [x/N for x in r_n]

        r_inh = []
        for i in range(t):
            r_inh.append(np.sum(output[Ntot:,i]))
        data_r['i'] = [x/self.Ni for x in r_inh]

        # Plot (smoothed) activation graphs
        for i in range(P):
            data_r[i] = np.convolve(data_r[i], np.ones(window)/window)
            ax2.plot(range(t),data_r[i][:t],label=str(i+1))

        data_r['i'] = np.convolve(data_r['i'], np.ones(window)/window)
        ax2.plot(range(t),data_r['i'][:t],c='black', alpha=0.8,label='inh')
        ax2.legend()
        ax1.matshow(output)
        ax1.set_aspect('auto')

        # ax2.set_title('Proportion of neurons active in subpopulations')

        plt.show()

        if show_mean:
            # Total activity (fraction of totatl time active)
            plt.figure()
            tot = {str(i+1): np.sum(data_r[i])/t for i in range(P)}
            plt.bar(tot.keys(),tot.values())
            plt.title('Mean activation per subpopulation')
            plt.show()

    def meanActivity_winning(self):
        """
        Counting the mean number of active neurons in each population when it is winning

        Input:
            * tSteps
            * V_temporal
            * bitsPerPatt

        Output:
            * activeNeurons -> PxtSteps -> number of active nuerons at each tStep
            * populationWinning -> PxtSteps -> where each position = 1 if this population was winning
            * num_tSteps_populationWinning -> 1xP -> sum of tSteps of each population winning
            * proportionActive -> 1xP -> mean number of active neurons when each population was winning
        """
        Vt = self.Vt
        P = self.P
        bitsPerPatt = self.Nep
        tSteps = Vt.shape[1]
        activeNeurons = np.zeros([P, tSteps])
        populationWinning = np.zeros([P, tSteps])

        for i in range(tSteps):

            for pop in range(P):
                activeNeurons[pop,i] = np.sum(Vt[pop*int(bitsPerPatt):(pop+1)*int(bitsPerPatt), i])

            currentPopulationWinning = np.argmax(activeNeurons[:, i])

            for pop in range(P):
                populationWinning[pop,i] = currentPopulationWinning == pop

        activeNeurons_populationWinning_total = np.sum(activeNeurons * populationWinning, 1)
        num_tSteps_populationWinning = np.sum(populationWinning, 1)

        proportionActive = activeNeurons_populationWinning_total / num_tSteps_populationWinning
        #proportionActive = np.divide(activeNeurons_populationWinning_total,
                                    # num_tSteps_populationWinning,
                                    # out=np.zeros_like(activeNeurons_populationWinning_total),
                                    # where=num_tSteps_populationWinning!=0)

        return activeNeurons, populationWinning, num_tSteps_populationWinning, proportionActive
