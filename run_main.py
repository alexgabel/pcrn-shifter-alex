import matplotlib
matplotlib.use('Agg') # Must be before importing matplotlib.pyplot or pylab!
import matplotlib.pyplot as plt

import shifter as sh
import time
import argparse
import numpy as np

def get_accuracy(W, h0=0.27, batch=20, amb=False,seed=None):
    score = 0
    np.random.seed(seed)
    for i in range(batch):
        V1, V2, V3, Vt, aN, pW, p, pA  = sh.combine_shifter_PCRN(W, num_samples=1,hExt0=h0,ambiguous=amb,verbose=False)
        if amb:
            if np.argmin(p) == np.argmin(V3[-1]):
                score += 1
            if (i+1)%5 == 0:
                print('Batch '+str(i+1)+'/'+str(batch)+' ('+str(score/(i+1)*100)+'%)')
        else:
            if np.argmax(p) == np.argmax(V3[-1]):
                score += 1
            if (i+1)%5 == 0:
                print('Batch '+str(i+1)+'/'+str(batch)+' ('+str(score/(i+1)*100)+'%)')
    print('Final score:',str(score/batch*100)+'%'+' ('+str(score)+'/'+str(batch)+')')
    print(10*'- ')
    return score/batch

parser=argparse.ArgumentParser(prog='sPCRN Learner',description='Learn W matrix with an sPCRN')
parser.add_argument('num_samples', type=int)
parser.add_argument('tSteps',type=int)
parser.add_argument('sweeps', type=int)
parser.add_argument('eval_interval', type=int)

args =parser.parse_args()

if __name__ == "__main__":

    t0 = time.time()
    W_hist = sh.learn_shifter_PCRN(num_samples=args.num_samples,
                              sweeps=args.sweeps,
                              tSteps=args.tSteps,
                              bits=10,
                              show_anything=False,
                              show_every=50,
                              keep_hist=True
                             )
    tf = time.time()
    runtime = tf-t0
    print('Total time: '+str(round(runtime,2))+' seconds')
    print('Training Done')

    eval_interval = args.eval_interval
    num_W = W_hist.shape[0]
    acc_list = []
    for i in range(0,num_W,eval_interval):
        print('Sweep: '+str(i))
        acc = get_accuracy(W_hist[i],batch=50,seed=42)
        acc_list.append(acc)

    s = str(args.num_samples)+'_'+str(args.tSteps)+'_'+str(args.sweeps)
    plt.figure()
    plt.scatter(range(0,num_W,eval_interval),acc_list)
    plt.savefig('connW_saved/eval_'+s+'.png',bbox_inches='tight')

    print('Evaluation Done')
