"""
Author: Sergi Mas-Pujol
Last update: 22/08/2019

The code requieres Python3.6
"""

import numpy as np
import math

def runNet(W, V, temperature, unClamped_indexes, num_iterations, hExt):
    """
    This function returns the Vt (state vector of the network) after the specified cycles

    Inpput:
        * W: learning from the shifter network
        * V: state vector of the network
        * temperature: temperature used in the sigmoid fucntion to determine if a nerour has to fire or not
        * unClamped_indexes: indexes of the neurons that will be eveluated to determine if they has to fire
        * num_iterations: how many cicly have to be applyed
        * hExt: add a specific external stimulation to the function

    Output:
        * V_temporal: State vector of the network after the cycles
        * h_storage: local field received by the output units along the cycles
    """

    V_temporal = np.zeros((num_iterations, V.shape[0]))

    numUnClamped = unClamped_indexes[1] - unClamped_indexes[0]
    k_stream = np.random.randint(unClamped_indexes[0], unClamped_indexes[1], numUnClamped*num_iterations)
    pk_toCompare = np.random.uniform(0, 1, numUnClamped*num_iterations)
    i_k = 0
    i_pk = 0

    dE = np.dot(W,V)
    dE = dE + hExt
    new_Vk = V
    for i in range(num_iterations):
        for probe in range(numUnClamped):
            k = k_stream[i_k]
            if np.log(1/pk_toCompare[i_pk]-1) > -dE[k]/temperature:
                new_Vk = 1
            else:
                new_Vk = 0

            deltaVk = new_Vk - V[k]
            if deltaVk != 0:
                dE += deltaVk*W[:,k]
                V[k] = new_Vk

            i_k += 1
            i_pk += 1

        V_temporal[i] = V

    return V_temporal

def runNet_flex(W, V, temperature, unClamped_indexes, num_iterations, hExt, bits,Ninhi=300):
    """
    This function returns the Vt (state vector of the network) after the specified cycles

    Inpput:
        * W: learning from the shifter network
        * V: state vector of the network
        * temperature: temperature used in the sigmoid fucntion to determine if a nerour has to fire or not
        * unClamped_indexes: indexes of the neurons that will be eveluated to determine if they has to fire
        * num_iterations: how many cicly have to be applyed
        * hExt: add a specific external stimulation to the function

    Output:
        * V_temporal: State vector of the network after the cycles
        * h_storage: local field received by the output units along the cycles
    """
    V_temporal = np.zeros((num_iterations, V.shape[0]))

    numUnClamped = unClamped_indexes[1] - unClamped_indexes[0]
    k_stream = np.random.randint(unClamped_indexes[0], unClamped_indexes[1], numUnClamped*num_iterations)

    k_stream[k_stream == (5*bits)] = 300+Ninhi+5*bits # fixes theta by replacing indices with final neuron
    # Assumes threshold index is after V1,V2,H (hidden units is 3*bits)

    pk_toCompare = np.random.uniform(0, 1, numUnClamped*num_iterations)
    i_k = 0
    i_pk = 0

    dE = np.dot(W,V)
    dE = dE + hExt
    new_Vk = V
    for i in range(num_iterations):
        for probe in range(numUnClamped):
            k = k_stream[i_k]
            if np.log(1/pk_toCompare[i_pk]-1) > -dE[k]/temperature:
                new_Vk = 1
            else:
                new_Vk = 0

            deltaVk = new_Vk - V[k]
            if deltaVk != 0:
                dE += deltaVk*W[:,k]
                V[k] = new_Vk

            i_k += 1
            i_pk += 1

        V_temporal[i] = V

    return V_temporal


def extract_hExt(W, V, temperature, unClamped_indexes, num_iterations, reductionW):
    """
    This function return the external stimulation or the local field received by
    the output units, and also, the Vt Vt after the specified  (new states of the neurons).
        The previous one, only returns the Vt after the specified cycles (new states of the neurons)

    Inpput:
        * W: learning from the shifter network
        * V: state vector of the network
        * temperature: temperature used in the sigmoid fucntion to determine if a nerour has to fire or not
        * unClamped_indexes: indexes of the neurons that will be eveluated to determine if they has to fire
        * num_iterations: how many cicly have to be applyed
        * reductionW: reduction in the weight from the learning

    Output:
        * V_temporal: State vector of the network after the cycles
        * h_storage: local field received by the output units along the cycles
    """

    V_temporal = np.zeros((num_iterations, V.shape[0]))
    numUnClamped = unClamped_indexes[1] - unClamped_indexes[0]
    h_storage = np.zeros((num_iterations, 3))

    k_stream = np.random.randint(unClamped_indexes[0], unClamped_indexes[1], numUnClamped*num_iterations)
    pk_toCompare = np.random.uniform(0, 1, numUnClamped*num_iterations)
    i_k = 0
    i_pk = 0

    dE = np.dot(W,V)
    new_Vk = V

    for i in range(num_iterations):
        it = iter(range(numUnClamped))
        for probe in it:
            k = k_stream[i_k]
            if pk_toCompare[i_pk] < 1/(1 + math.exp(-dE[k]/temperature)):
                new_Vk = 1
            else:
                new_Vk = 0

            deltaVk = new_Vk - V[k]
            if deltaVk != 0:
                dE += deltaVk*W[:,k]
                V[k] = new_Vk

            i_k += 1
            i_pk += 1

        V_temporal[i] = V

        """ W for collecting data """
        W_collectingData = reductionW*W # Reduction of the values used to extract hExt
        aux = np.where(V == 1)
        dE_forHext = np.dot(W_collectingData,V) # Computing the sum of all the elements (WITH threshold)

        for z in range(h_storage.shape[1]): # Evaluating the three bits of V3
            h_storage[i][z] = dE_forHext[unClamped_indexes[0]+z]

    return V_temporal, h_storage
