"""
Author: Sergi Mass-Pujol
Last Modification: 22/08/2019

This file contains the different types of samples can be used/generated/introduced to the shifter network

    * Unambiguos samples
    * Maximal ambiguos samples
    * Biased ambiguous samples
"""


import numpy as np
from scipy import sparse
import matplotlib.pyplot as plt

import math
from multiprocessing import Pool


# ================================================================
# Create input examples for the learning (shifter network - Boltzmann machine)
# ================================================================

def samples(units, N):

    V1 = np.zeros((N,units.get("bits_V1")))
    V2 = np.zeros((N,units.get("bits_V2")))
    V3 = np.zeros((N,units.get("bits_V3")))
    shift = np.zeros((N,1))

    if units.get("bits_V1") <= 8:
        probs = [2./3, 1./3] # For 8 bits or less -> P(on) = 0.3
    else:
        probs = [3./5, 2./5] # For larger than 8 (e.g. 10 bits) -> P(on) = 0.4

    for j in range(N):
        v1 = np.random.choice([0, 1],
        # Prob(on) = 1/3 (initial) -> p=[2./3, 1./3])
        size=(units.get("bits_V1"),), p=probs)
        v2 = np.zeros(units.get("bits_V2"))
        v3 = np.zeros(units.get("bits_V3"))
        shift[j] = np.random.randint(0,3)

        if shift[j] == 0: # left
            for i in range(units.get("bits_V1")):
                v2[i] = v1[(i+1)%units.get("bits_V1")]
                v3[0] = 1
        elif shift[j] == 2: # right
            for i in range(units.get("bits_V1")):
                v2[i] = v1[(i-1)%units.get("bits_V1")]
                v3[2] = 1
        elif shift[j] == 1: # noShift
            for i in range(units.get("bits_V1")):
                v2[i] = v1[i]
                v3[1] = 1

        V1[j] = v1
        V2[j] = v2
        V3[j] = v3

    return V1, V2, V3

# ================================================================
# Unambiguous samples
# ================================================================
def unambiguous(seed, units, N_samples, N_bitsOn):
    """
    This function generates input examples without ambiguty where the shift inse the examples is selected randomly

    Input:
        * seed: seed used to generate the random number for the activation of the bits
        * units = Dictionary with the main characteristics of the implementation
        * N_samples = Total number of sample you want to generate
        * N_bitsOn = How many bits will be on in the generated samples
    Output:
        * Randomly chosen the bits ON and the applyed shift
            * V1 -> Size [N_samples, 10]
            * V2 -> Size [N_samples, 10]
            * V3 -> Size [N_samples, 3]
    """

    #np.random.seed(seed) # Seed used to generate the samples

    """ Setting characteristics of the samples for the predicitons """

    V1 = np.zeros((N_samples,units.get("bits_V1")))
    V2 = np.zeros((N_samples,units.get("bits_V2")))
    V3 = np.zeros((N_samples,units.get("bits_V3")))

    # Only works for ONE example
    if (N_bitsOn == 0):
        return V1, V2, V3

    shift = np.zeros((N_samples,units.get("threshold")))

    for j in range(N_samples):
        k = np.random.choice(10, N_bitsOn, replace=False)
#         k = np.random.choice(8, N_bitsOn, replace=False) # Use it to create input vector of 8 bits

        np.put(V1[j], k, np.ones(k.shape[0]))
        v2 = np.zeros(units.get("bits_V2"))
        v3 = np.zeros(units.get("bits_V3"))
        shift[j] = np.random.randint(0,3)

        if shift[j] == 0: # left
            for i in range(units.get("bits_V1")):
                v2[i] = V1[j][(i+1)%units.get("bits_V1")]
                v3[0] = 1
        elif shift[j] == 2: # right
            for i in range(units.get("bits_V1")):
                v2[i] = V1[j][(i-1)%units.get("bits_V1")]
                v3[2] = 1
        elif shift[j] == 1: # noShift
            for i in range(units.get("bits_V1")):
                v2[i] = V1[j][i]
                v3[1] = 1

        V2[j] = v2
        V3[j] = v3

    return V1, V2, V3


def unambiguous_shiftFixed(seed, units, N_samples, N_bitsOn, shift):
    """
    This function generates input examples without ambiguty containg the specified shift.
    All the examples will contain the same type of shift

    Input:
        * seed: seed used to generate the random number for the activation of the bits
        * units = Dictionary with the main characteristics of the implementation
        * N_samples = Total number of sample you want to generate
        * N_bitsOn = How many bits will be on in the generated samples
        * shift = 0 -> left;
                  1-> no shift ;
                  2 -> right
    Output:
        * Randomly chosen the bits ON (only)
            * V1 -> Size [N_samples, 10]
            * V2 -> Size [N_samples, 10]
            * V3 -> Size [N_samples, 3]
    """

    """ Setting characteristics of the samples for the predicitons """
    np.random.seed(seed) # Seed used to generate the samples

    V1 = np.zeros((N_samples,units.get("bits_V1")))
    V2 = np.zeros((N_samples,units.get("bits_V2")))
    V3 = np.zeros((N_samples,units.get("bits_V3")))
    shift = np.zeros((N_samples,units.get("threshold"))) + shift # Adding the corresponding value associated to the shift

    for j in range(N_samples):
        k = np.random.choice(10, N_bitsOn, replace=False)
#         k = np.random.choice(8, N_bitsOn, replace=False) # # Use it to create input vector of 8 bits

        np.put(V1[j], k, np.ones(k.shape[0]))
        v2 = np.zeros(units.get("bits_V2"))
        v3 = np.zeros(units.get("bits_V3"))

        if shift[j] == 0: # left
            for i in range(units.get("bits_V1")):
                v2[i] = V1[j][(i+1)%units.get("bits_V1")]
                v3[0] = 1
        elif shift[j] == 2: # right
            for i in range(units.get("bits_V1")):
                v2[i] = V1[j][(i-1)%units.get("bits_V1")]
                v3[2] = 1
        elif shift[j] == 1: # noShift
            for i in range(units.get("bits_V1")):
                v2[i] = V1[j][i]
                v3[1] = 1

        V2[j] = v2
        V3[j] = v3

    return V1, V2, V3


# ================================================================
# Maximal ambiguous samples
# ================================================================

def moveLeft(units, v1, elementsToMove):
    v2 = np.zeros(units.get("bits_V2"))
    v3 = np.zeros(units.get("bits_V3"))
    for i in elementsToMove:
        # v2[i-1] = v1[i]
        v2[i-1] = v1[i%units.get("bits_V1")]
        v3[0] = 1

    return v2, v3

def moveRight(units, v1, elementsToMove):
    v2 = np.zeros(units.get("bits_V2"))
    v3 = np.zeros(units.get("bits_V3"))
    for i in elementsToMove:
        # v2[i+1] = v1[i]
        v2[i+1] = v1[i%units.get("bits_V1")]
        v3[2] = 1

    return v2, v3

def noMove(units, v1, elementsToMove):
    v2 = np.zeros(units.get("bits_V2"))
    v3 = np.zeros(units.get("bits_V3"))
    for i in elementsToMove:
        v2[i] = v1[i]
        v3[1] = 1

    return v2, v3

def maximalAambiguous(seed, units, N_samples, N_bitsOn):
    """
    This function generates input examples with maximal ambiguty containg random shifts.
        All combination will appear in the set of vectors.

    Input:
        * seed: seed used to generate the random number for the activation of the bits
        * units = Dictionary with the main characteristics of the implementation
        * N_samples = Total number of sample you want to generate
        * N_bitsOn = How many bits will be on in the generated samples
    Output:
        * Randomly chosen the bits ON and the applyed shift
            * V1 -> Size [N_samples, 10]
            * V2 -> Size [N_samples, 10]
            * V3 -> Size [N_samples, 3]
    """
    np.random.seed(seed) # Seed used to generate the sample

    deletedSamples = 0

    V1 = np.zeros((N_samples,units.get("bits_V1")))
    V2 = np.zeros((N_samples,units.get("bits_V2")))
    V3 = np.zeros((N_samples,units.get("bits_V3")))

    for j in range(N_samples):
        possiblePossitions = list(range(1,units.get("bits_V1")-1))
        k = np.random.choice(possiblePossitions, N_bitsOn, replace=False)

        # Sorting the random number
        k.sort()

        np.put(V1[j], k, np.ones(k.shape[0]))

        # Dividing the bits on into two sets
        elementsInSet1 = math.floor(k.shape[0]/2)
        elementsInSet2 = N_bitsOn - elementsInSet1

         # Allowing random order when the number of bits ON is ODD
        order = order = np.random.randint(2)
        if (order == 0):
            set1 = k[0:elementsInSet1]
            set2 = k[elementsInSet1:]
        else:
            set2 = k[0:elementsInSet1]
            set1 = k[elementsInSet1:]

        # Randomly chossing the two shift
        possibleShift = [0, 1, 2]
        shifts = np.random.choice(possibleShift, 2, replace=False)

        i = 0 # To use the two possible sets
        v2 = np.zeros(units.get("bits_V2"))
        v3 = np.zeros(units.get("bits_V3"))
        for shift in shifts:
            # Spliting the sets for the two shifts
            if i == 0:
                setX = k[0:elementsInSet1]
            else:
                setX = k[elementsInSet1:]

            # Left shift
            if shift == 0:
                v20, v30 = moveLeft(units, V1[j], setX)
                v2 += v20
                v3 += v30
                i+=1

            # No shift
            if shift == 1:
                v21, v31 = noMove(units, V1[j], setX)
                v2 += v21
                v3 += v31
                i+=1

            # Right shift
            if shift == 2:
                v22, v32 = moveRight(units, V1[j], setX)
                v2 += v22
                v3 += v32
                i+=1

        V2[j] = v2
        V3[j] = v3

        # Raw stimations of the deleted samples
        if np.max(V2[j]) == 2 :
            deletedSamples +=1

    # Delete sample that present overlaping
    V1 = np.delete(V1, np.where(V2 == 2)[0], axis=0)
    V3 = np.delete(V3, np.where(V2 == 2)[0], axis=0)
    V2 = np.delete(V2, np.where(V2 == 2)[0], axis=0)

    print("Deleted samples", deletedSamples)
    print(shifts)
    return V1, V2, V3


def maximalAambiguous_shiftFixed(seed, units, N_samples, N_bitsOn, shifts):
    """
    This function generates input examples with maximal ambiguty containg rthe specifid shifts.
        Only the specified shufts will appear in the set of vectors.

    Input:
        * seed: seed used to generate the random number for the activation of the bits
        * units = Dictionary with the main characteristics of the implementation
        * N_samples = Total number of sample you want to generate
        * N_bitsOn = How many bits will be on in the generated samples
		* shift -> Size [1, 2] where the first position corresponds with the first applyed shift
			0 -> left
          	1-> no shift
          	2 -> right
    Output:
        * Randomly chosen the bits ON (only)
            * V1 -> Size [N_samples, 10]
            * V2 -> Size [N_samples, 10]
            * V3 -> Size [N_samples, 3]
    """

    """ Setting characteristics of the samples for the predicitons """
    np.random.seed(seed) # Seed used to generate the samples

    deletedSamples = 0

    V1 = np.zeros((N_samples,units.get("bits_V1")))
    V2 = np.zeros((N_samples,units.get("bits_V2")))
    V3 = np.zeros((N_samples,units.get("bits_V3")))

    for j in range(N_samples):
        possiblePossitions = list(range(1,units.get("bits_V1")-1))
        # possiblePossitions = list(range(0,units.get("bits_V1")))

        k = np.random.choice(possiblePossitions, N_bitsOn, replace=False)

        # Sorting the random number
        k.sort()

        np.put(V1[j], k, np.ones(k.shape[0]))

        # Dividing the bits on into two sets
        elementsInSet1 = math.floor(k.shape[0]/2)
        elementsInSet2 = N_bitsOn - elementsInSet1

        # Allowing random order when the number of bits ON is ODD
        order = order = np.random.randint(2)
        if (order == 0):
            set1 = k[0:elementsInSet1]
            set2 = k[elementsInSet1:]
        else:
            set2 = k[0:elementsInSet1]
            set1 = k[elementsInSet1:]

        i = 0 # To use the two possible sets
        v2 = np.zeros(units.get("bits_V2"))
        v3 = np.zeros(units.get("bits_V3"))
        for shift in shifts:
            # Spliting the sets for the two shifts
            if i == 0:
                setX = k[0:elementsInSet1]
            else:
                setX = k[elementsInSet1:]

            # Left shift
            if shift == 0:
                v20, v30 = moveLeft(units, V1[j], setX)
                v2 += v20
                v3 += v30
                i+=1

            # No shift
            if shift == 1:
                v21, v31 = noMove(units, V1[j], setX)
                v2 += v21
                v3 += v31
                i+=1

            # Right shift
            if shift == 2:
                v22, v32 = moveRight(units, V1[j], setX)
                v2 += v22
                v3 += v32
                i+=1

        V2[j] = v2
        V3[j] = v3

        # Raw stimations of the deleted samples
        if np.max(V2[j]) == 2 :
            deletedSamples +=1

    # Delete sample that present overlaping
    V1 = np.delete(V1, np.where(V2 == 2)[0], axis=0)
    V3 = np.delete(V3, np.where(V2 == 2)[0], axis=0)
    V2 = np.delete(V2, np.where(V2 == 2)[0], axis=0)

    print("Deleted samples", deletedSamples)
    print(shifts)
    return V1, V2, V3


# ================================================================
#  Bias ambiguous samples
# ================================================================

"""
# "f" units shifted one way, "1-f" the other way
# Our implementation use directly the NUMBER of bit used in the FIRST SHIFT
"""
def biasAambiguous(seed, units, N_samples, N_bitsOn, f):
    """
    This function generates input examples with bias ambiguty containg random combinations of shifts.
        All combinations of shifts will appear in the set of vectors.

    Input:
        * seed: seed used to generate the random number for the activation of the bits
        * units = Dictionary with the main characteristics of the implementation
        * N_samples = Total number of sample you want to generate
        * N_bitsOn = How many bits will be on in the generated samples
		* f = number of bits shifted in the first selected shift
    Output:
        * Randomly chosen the bits ON and the applyed shift
            * V1 -> Size [N_samples, 10]
            * V2 -> Size [N_samples, 10]
            * V3 -> Size [N_samples, 3]
    """

    np.random.seed(seed) # Seed used to generate the sample

    deletedSamples = 0

    V1 = np.zeros((N_samples,units.get("bits_V1")))
    V2 = np.zeros((N_samples,units.get("bits_V2")))
    V3 = np.zeros((N_samples,units.get("bits_V3")))

    for j in range(N_samples):
        possiblePossitions = list(range(1,units.get("bits_V1")-1))
        k = np.random.choice(possiblePossitions, N_bitsOn, replace=False)

        # Sorting the random number
        k.sort()

        np.put(V1[j], k, np.ones(k.shape[0]))

        # Dividing the bits on into two sets
        elementsInSet1 = f
        elementsInSet2 = N_bitsOn - f

        # Allowing random order when the number of bits ON is ODD
        order = order = np.random.randint(2)
        if (order == 0):
            set1 = k[0:elementsInSet1]
            set2 = k[elementsInSet1:]
        else:
            set2 = k[0:elementsInSet1]
            set1 = k[elementsInSet1:]

        # Randomly chossing the two shift
        possibleShift = [0, 1, 2]
        shifts = np.random.choice(possibleShift, 2, replace=False)

        i = 0 # To use the two possible sets
        v2 = np.zeros(units.get("bits_V2"))
        v3 = np.zeros(units.get("bits_V3"))
        for shift in shifts:
            # Spliting the sets for the two shifts
            if i == 0:
                setX = k[0:elementsInSet1]
            else:
                setX = k[elementsInSet1:]

            # Left shift
            if shift == 0:
                v20, v30 = moveLeft(units, V1[j], setX)
                v2 += v20
                v3 += v30
                i+=1

            # No shift
            if shift == 1:
                v21, v31 = noMove(units, V1[j], setX)
                v2 += v21
                v3 += v31
                i+=1

            # Right shift
            if shift == 2:
                v22, v32 = moveRight(units, V1[j], setX)
                v2 += v22
                v3 += v32
                i+=1

        V2[j] = v2
        V3[j] = v3

        # Raw stimations of the deleted samples
        if np.max(V2[j]) == 2 :
            deletedSamples +=1

    # Delete sample that present overlaping
    V1 = np.delete(V1, np.where(V2 == 2)[0], axis=0)
    V3 = np.delete(V3, np.where(V2 == 2)[0], axis=0)
    V2 = np.delete(V2, np.where(V2 == 2)[0], axis=0)


    print("Deleted samples", deletedSamples)
    print(shifts)
    return V1, V2, V3


def biasAambiguous_shiftFixed(seed, units, N_samples, N_bitsOn, f, shifts):
    """
    This function generates input examples with bias ambiguty containg the specified shiftss.
        Only the specified shifts will appear in the set of vectors.

    Input:
        * seed: seed used to generate the random number for the activation of the bits
        * units = Dictionary with the main characteristics of the implementation
        * N_samples = Total number of sample you want to generate
        * N_bitsOn = How many bits will be on in the generated samples
		* f = number of bits shifted in the first selected shift
		* shift -> Size [1, 2] where the first position corresponds with the first specified shift
			0 -> left
          	1-> no shift
          	2 -> right
    Output:
        * Randomly chosen the bits ON and the applyed shift
            * V1 -> Size [N_samples, 10]
            * V2 -> Size [N_samples, 10]
            * V3 -> Size [N_samples, 3]
    """

    """ Setting characteristics of the samples for the predicitons """
    np.random.seed(seed) # Seed used to generate the samples

    deletedSamples = 0

    V1 = np.zeros((N_samples,units.get("bits_V1")))
    V2 = np.zeros((N_samples,units.get("bits_V2")))
    V3 = np.zeros((N_samples,units.get("bits_V3")))

    for j in range(N_samples):
        possiblePossitions = list(range(1,units.get("bits_V1")-1))
        k = np.random.choice(possiblePossitions, N_bitsOn, replace=False)

        # Sorting the random number
        k.sort()

        np.put(V1[j], k, np.ones(k.shape[0]))

        # Dividing the bits on into two sets
        elementsInSet1 = f
        elementsInSet2 = N_bitsOn-f

        # Allowing random order when the number of bits ON is ODD
        order = order = np.random.randint(2)
        if (order == 0):
            set1 = k[0:elementsInSet1]
            set2 = k[elementsInSet1:]
        else:
            set2 = k[0:elementsInSet1]
            set1 = k[elementsInSet1:]

        i = 0 # To use the two possible sets
        v2 = np.zeros(units.get("bits_V2"))
        v3 = np.zeros(units.get("bits_V3"))
        for shift in shifts:
            # Spliting the sets for the two shifts
            if i == 0:
                setX = k[0:elementsInSet1]
            else:
                setX = k[elementsInSet1:]

            # Left shift
            if shift == 0:
                v20, v30 = moveLeft(units, V1[j], setX)
                v2 += v20
                v3 += v30
                i+=1

            # No shift
            if shift == 1:
                v21, v31 = noMove(units, V1[j], setX)
                v2 += v21
                v3 += v31
                i+=1

            # Right shift
            if shift == 2:
                v22, v32 = moveRight(units, V1[j], setX)
                v2 += v22
                v3 += v32
                i+=1

        V2[j] = v2
        V3[j] = v3

        # Raw stimations of the deleted samples
        if np.max(V2[j]) == 2 :
            deletedSamples +=1

    # Delete sample that present overlaping
    V1 = np.delete(V1, np.where(V2 == 2)[0], axis=0)
    V3 = np.delete(V3, np.where(V2 == 2)[0], axis=0)
    V2 = np.delete(V2, np.where(V2 == 2)[0], axis=0)


    print("Deleted samples", deletedSamples)
    print(shifts)
    return V1, V2, V3
